# jimdoyle.com.au

## Description
Portfolio site using Bootstrap 3 and Sass. Started in June 2014. 

## Mobile screen shots
http://ipadpeek.com/

## Apache and .htaccess

### Caching
Using expires headers, based on this reference:
http://www.feedthebot.com/pagespeed/leverage-browser-caching.html
Plus "grunt-cache-bust" task to rename resources on each build.

### GZIP
Ref: http://www.feedthebot.com/pagespeed/enable-compression.html

### .htc files
Uses `AddType text/x-component .htc`.

TODO:
High priority:
- Add stack overflow icon to contact
- Add videos for EasterShow https://vimeo.com/180995612/dd611a39f6
- get rid of all snug boxes, as their display: table styles don't work well on mobile width
- update Toyota Playground with more screens
- add articles: http://www.crn.com.au/news/how-the-sydney-royal-easter-show-migrated-to-microsoft-azure-440882

Low priority
- rearrange bg icons so edges show most important ones
- (saved in assets/bg-icons/new) add stack overflow, webpack, susy, UIKit, TypeScript and Babel bg icons
- Add more screens to Corolla Good Times / AFL Ledgendary Moments
- add a lightbox to enlarge the carousel images
- add nav "archive", "demos" (for github libs)
- add archive section and include previous Flash websites
- remove unused BS sass & plugins
- Add unsupported message for ie8 (maybe just use existing browserhappy one, but hide everything else)
- Animate bg icons on certain interactions


DONE:
- update oaks with carousel
- update Dan Murphys with carousel
- add Garden Flip game on a separate page
- remove Sunshine FDC
- remove Glyphicons and just use SVGs
- merge Corrolla and AFL and explain how they were based on the same AngularJS directives.
- add ShowGround
- fix white bg height bug when "read more" clicked
- extend bg to full height using flex box
- add task for copying svg symbols to hbs file
- remove copyright
- add gulp svg-sprite and use that instead of icon font
- remove the "i" popover
- Use handlebars for projects
- add Yeoman logo
- remove the "=" in title
- make intro text larger
- use Open Sans for body text (google fonts)
- add Oaks Mobile (m.oakshotelsresorts.com)
- add Easter Show