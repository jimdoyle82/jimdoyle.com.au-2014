module.exports = function(grunt) {



var autoPrefixerBrowsers = [
    // Desktop
      'last 3 Chrome versions'
    , 'last 2 Firefox versions'
    , 'last 2 Safari versions'
    , 'last 2 Edge versions'
    , 'ie >= 9'
    // Mobile
    , 'last 3 ChromeAndroid versions'
    , 'last 3 Android versions'
    , 'last 3 FirefoxAndroid versions'
    , 'last 3 iOS versions'
    , 'last 2 ExplorerMobile versions'
    , 'last 2 OperaMobile versions'
    // Other
    , '> 2% in AU'
],
autoprefixer = require('autoprefixer'), // add vendor prefixes
cssnano = require('cssnano'); // minify the result

    grunt.initConfig({
        pkg: grunt.file.readJSON("../../package.json")

        ,vars: {
            root:   "../../"
            ,dev:   "<%= vars.root %>_dev/"
            ,dist:  "<%= vars.root %>_dist/"
            ,deploy:"<%= vars.dist %>deploy/"
            ,app:   "<%= vars.dev %>app/"
            ,externals:         "<%= vars.dev %>deps/externals/"
            ,dist_css_debug:    "<%= vars.deploy %>css/debug/"
            ,dist_css_min:      "<%= vars.deploy %>css/min/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd") + ' - <%= pkg.author.name %>'
        }


        ,clean: {
            options: { force: true }
            ,debug: [ "<%= vars.dist_css_debug %>*" ]
            ,min:   [ "<%= vars.dist_css_min %>*" ]
        }


        ,watch: {
            debug: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:debug", "forcereload" ]
            }
            // TODO
            ,min: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:min" ]
            }
            ,both: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:debug", "sass:min" ]
            }
        }

        ,sass: {
            debug: {
                //"nested, expanded, compact, compressed"
                options: { style: "expanded" }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: "<%= vars.app %>"
                    ,src: [   "scss/*.scss" ]
                    ,dest: "<%= vars.dist_css_debug %>"
                    ,ext: '.css'
                }]
            },

            
            min: {
                //"nested, expanded, compact, compressed"
                options: { style: "compressed" }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: "<%= vars.app %>"
                    ,src: [   "scss/*.scss" ]
                    ,dest: "<%= vars.dist_css_min %>"
                    ,ext: '.css'
                }]
            },

            svgsprite: {
                options: { style: "expanded" }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: "<%= vars.dist %>svg/view/"
                    ,src: [   "sprite.scss" ]
                    ,dest: "<%= vars.dist_css_debug %>"
                    ,ext: '.css'
                }]
            }
        }

        ,postcss: {

            prefix: {
                options: {
                  processors: [
                    autoprefixer({browsers: autoPrefixerBrowsers})
                  ]
                },
                dist: {
                  src: "<%= vars.dist_css_debug %>*.css"
                }
            },
            /*
            min: {
                options: {
                  map: {
                      inline: false, // save all sourcemaps as separate files...
                      annotation: 'dist/css/maps/' // ...to the specified directory
                  },
                  processors: [
                    cssnano()
                  ]
                },
                dist: {
                  src: 'css/*.css'
                }
            }
            */
        }

    });

    
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');


    grunt.registerTask("forcereload", function() {

        console.log( "Changing watched file: " + grunt.config("vars.dist") + "/forcereload.txt" );
        grunt.file.write( grunt.config("vars.dist") + "/forcereload.txt", Math.random() );
    });


    grunt.registerTask("default", function( option ) {

        /**
         * Options to configure your scss compile, using Grunt Option.
         * Don't use more than 1 option.
         * Eg `grunt --min`.
         * The 'option' argument may also be used like this `grunt default:min`.
         */

        var opts = {}

        if( option ) {
            opts[ option ] = true;
        } else {
            
            // straight compiles
            opts["debug"]  = !!grunt.option("Debug")  // just debug (Cap "D" because --debug is reserved by Grunt)
            opts["min"]    = !!grunt.option("min")    // just min
            opts["both"]   = !!grunt.option("both");  // both

            // watches
            opts["wbug"] = !!grunt.option("watch.debug")   // just watch debug
            opts["wmin"]   = !!grunt.option("watch.min")   // just watch min
            opts["wboth"]  = !!grunt.option("watch.both"); // watch both
        }

        var tasks = [];

        if( opts["debug"] )    tasks = [ "clean:debug", "sass:debug" ];
        if( opts["min"] )      tasks = [ "clean:min",   "sass:min" ];
        if( opts["both"] )     tasks = [ "clean:min", "clean:debug", "sass:min", "sass:debug" ];

        if( opts["wbug"] )      tasks = [ "clean:debug", "sass:debug", "watch:debug" ];
        if( opts["wmin"] )      tasks = [ "clean:min",   "sass:min",   "watch:min" ];
        if( opts["wboth"] )     tasks = [ "clean:min", "clean:debug", "sass:debug", "sass:min", "watch:both" ];

        tasks.push("postcss:prefix");

        grunt.task.run( tasks );


        // only write out list if no options passed
        if( tasks.length > 0 ) return;

        /**
         * Writes out a list of options for this project to the command-line.
         */

        grunt.log.header( "Hi there. What would you like to run?".yellow.bold );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --Debug".cyan );
        grunt.log.writeln( "Will compile to debug/expanded css.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --min".cyan );
        grunt.log.writeln( "Will compile to minified css.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --both".cyan );
        grunt.log.writeln( "Will compile to both debug & minified css.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --watch.debug".cyan );
        grunt.log.writeln( "Will run ".grey + "--Debug".yellow + ", then watch.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --watch.min".cyan );
        grunt.log.writeln( "Will run ".grey +"--min".yellow +", then watch.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --watch.both".cyan );
        grunt.log.writeln( "Will run ".grey + "--both".yellow + ", then watch.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "NB:".yellow );
        grunt.log.writeln( "Don't use more than 1 option.".yellow );
        grunt.log.writeln( "--debug".yellow + " is a reserved keyword, so using ".grey + "--Debug".yellow +" (Cap 'D') instead.".yellow );

    });
};
