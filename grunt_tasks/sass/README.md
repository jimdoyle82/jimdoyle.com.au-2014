# GruntJS build for scss in an Requangular (RequireJS / AngularJS) web app
To run this script, type into command line one of these options:
`grunt`

This will place a human readable css, based on "development/scss/bootstrap.scss", file in "_example/css/bootstrap.css".

# Gotchas
## Windows 360 character limit and Sprites
`grunt-contrib-compass` was removed, as sprites are only needed for the main app and not sub-modules. Also, it had issues in Windows OSs <br>
when deeply nested inside lots of directories, due to the 260 character limit of Windows and the `sass-cache` long folder names. I (JimD) couldn't <br>
find a solution using `grunt-contrib-compass`, however, using `grunt-contrib-sass` there was an option to move the location of the `sass-cache` folder. <br>
So, I've moved to `/tmp/sass-cache` at the user's root, so that there are no long path issues. It is still possible to use compass sprites by <br>
editing the `config.rb` file.


# Dependencies
Sass 3.2.17 (not higher)
Compass 0.12.2 (not higher)