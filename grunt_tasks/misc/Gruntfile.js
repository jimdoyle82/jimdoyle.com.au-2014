
module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("../../package.json")

        ,vars: {
            root:           "../../"
            ,dev:           "<%= vars.root %>_dev/"
            ,dist:          "<%= vars.root %>_dist/"
            ,deploy:        "<%= vars.dist %>deploy/"
            ,app:           "<%= vars.dev %>app/"
            ,deps:          "<%= vars.dev %>deps/"
            ,bowcomp:       "<%= vars.deps %>bower_components/"
            ,versioned:     "<%= vars.deps %>libs/versioned/"
            ,dist_img_singles: "<%= vars.dist %>img/singles-uncompressed/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd") + ' - <%= pkg.author.name %>'
        }


        ,copy: {
            // we don't need to deploy these because thay come from CDN, so they just get copied to dist
            fonts: {
                expand: true
                ,flatten: false
                ,cwd: "<%= vars.dev %>app/fonts/"
                ,src: [  "**/*" ]
                ,dest: "<%= vars.dist %>fonts/" 
            }

            // copies deps into _dist/deploy that DO already have minified versions
            ,libs: {
                expand: true
                ,flatten: true
                ,cwd: "<%= vars.deps %>"
                ,src: [ "bower_components/background-size-polyfill/backgroundsize.min.htc"
                        ,"libs/array-every-polyfill/array-every-polyfill.min.js"
                        ,"libs/array-indexof-polyfill/array-indexof-polyfill.min.js"
                        ,"libs/classlist-polyfill/classlist.min.js"
                        ,"libs/eventlistener-polyfill/eventlistener.polyfill.min.js"
                        ,"libs/respondjs/respond.min.js"
                        ,"bower_components/html5shiv/dist/html5shiv-printshiv.js"
                        ,"bower_components/html5shiv/dist/html5shiv.js"
                        ,"libs/multicolumn-polyfill/multicolumn.js"
                        ,"bower_components/jquery/dist/jquery.min.js"
                    ]
                ,dest: "<%= vars.deploy %>libs/"
            }
            ,versioned: {
                expand: true
                ,flatten: false
                ,cwd: "<%= vars.deps %>libs/versioned/"
                ,src: ["**/*"]
                ,dest: "<%= vars.deploy %>libs/versioned/"
            }
            // copies all non-sprite images
            ,appimg: {
                 expand: true
                ,flatten: true
                ,cwd: "<%= vars.dev %>"
                ,src: [  "app/img/singles/*.{png,gif,jpg}" ]
                ,dest: "<%= vars.dist_img_singles %>"
            }
            // copies app js
            /*,appjs: {
                 expand: true
                ,flatten: true
                ,cwd: "<%= vars.app %>js/"
                ,src: [  "App.js" ]
                ,dest: "<%= vars.deploy %>js/"
            }*/

            ,"svg-to-hbs": {
                src: "<%= vars.dist %>svg/symbol/svg/sprite.symbol.svg"
                ,dest: "<%= vars.app %>hbs/svgsymbols.hbs"
            }
        }


        ,clean: {
            options: { force: true }
            ,libs:      [ "<%= vars.deploy %>libs/*" ]
            ,html:      [ "<%= vars.deploy %>index.html" ]
        }


        ,uglify: {
            options: {
                 preserveComments: "some"
                ,banner: '/** <%= vars.banner_content %> */\n'
                ,compress: {
                    global_defs: { "DEBUG": false }
                }
            },
            libs: {
                files: [{
                            src: '<%= vars.deploy %>js/app.debug.js'
                            ,dest: '<%= vars.deploy %>js/app.min.js'
                            
                        },{
                            src: '<%= vars.bowcomp %>json2/json2.js'
                            ,dest: '<%= vars.deploy %>libs/json2.min.js'
                        }]
            }
        }


        ,imagemin: {

            // sprites already get compressed in "sass" task

            singles: {
                options: {
                    pngquant: true
                    ,optimizationLevel: 3
                }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: '<%= vars.dist_img_singles %>'
                    ,src: ['*.{png,jpg,gif}']
                    ,dest: '<%= vars.deploy %>img/'
                }]
            }
        }

        ,concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [  
                        '<%= vars.deps %>libs/modernizr.custom.93380.js'
                        ,'<%= vars.deps %>bower_components/lodash/dist/lodash.js'
                        ,"<%= vars.deps %>bower_components/slick-carousel/slick/slick.js"
                        ,'<%= vars.deps %>libs/bootstrap-3.1.1/js/transition.js'
                        ,"<%= vars.deps %>libs/bootstrap-3.1.1/js/collapse.js"
                        ,"<%= vars.deps %>libs/bootstrap-3.1.1/js/tooltip.js"
                        ,"<%= vars.deps %>libs/bootstrap-3.1.1/js/popover.js"
                        ,'<%= vars.app %>js/Utils.js'
                        ,'<%= vars.app %>js/App.js'
                    ]
                ,dest: '<%= vars.deploy %>js/app.debug.js'
            }
        }

        // make sure you only remove console methods from the debug version (before minification) or else it won't work
        ,removelogging: {
            dist: {
                options: {
                    replaceWith: 0 // replaces with 0 so that "if" statements have something to compare
                    ,methods: [ "log" ] // keeps all console statements except for "log"
                }
                ,src: '<%= vars.deploy %>js/app.debug.js'
                ,dest: '<%= vars.deploy %>js/app.debug.js'
            }
        }

        ,htmlmin: {
            dist: {
                options: {
                      removeComments: true
                    , collapseWhitespace: true
                    , keepClosingSlash: true // breaks the svgs so must be enabled
                }
                ,files: {
                    "<%= vars.deploy %>index.html": "<%= vars.deploy %>index-uncompressed.html"
                }
            }
        }

        ,cacheBust: {
            options: {
              rename: true // do rename files
              ,dir: "<%= vars.deploy %>" // base url to look for assets
              ,ignorePatterns: ["libs"] // libs shouldn't be changing, so ignore them
            },
            assets: {
                files: [{
                    src: ['<%= vars.deploy %>index.html']
                }]
            }
        }

        ,"compile-handlebars": {
            app: {
                src: '<%= vars.app %>index.hbs',
                dest: '<%= vars.deploy %>index.html',
                partials: '<%= vars.app %>hbs/*.hbs'
                // globals: [{
                //     imgPath:"/_dist/deploy/img/" 
                // }]
            }
        }


        ,svg_sprite: {
            app: {
                expand      : true,
                cwd         : '<%= vars.app %>img/svgsprite/',
                src         : ['*.svg'],
                dest        : '<%= vars.dist %>svg/',
                options     : {
                    mode                : {
                        symbol          : true      // Activate the «symbol» mode 
                        // defs            : true
                    },

                    shape               : {
                        dimension       : {         // Set maximum dimensions 
                            maxWidth    : 32,
                            maxHeight   : 32
                        },
                        spacing         : {         // Add padding 
                            padding     : 10
                        },
                        transform       : [
                            {svgo       : {
                                plugins : [
                                    // {transformsWithOnePath: false},
                                    // {moveGroupAttrsToElems: false}
                                    // {collapseGroups: false}
                                     { convertPathData: false }
                                    ,{ convertStyleToAttrs: false }
                                    ,{ convertTransform: false }
                                    ,{ convertShapeToPath: false }
                                ]
                            }}
                        ]
                    }
                }
            }
        }
    });
        

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-remove-logging');
    grunt.loadNpmTasks('grunt-cache-bust');
    grunt.loadNpmTasks('grunt-compile-handlebars');
    grunt.loadNpmTasks('grunt-svg-sprite');

    grunt.registerTask("default", [ "clean"
                                    ,"copy"
                                    ,"concat"
                                    ,"removelogging"
                                    ,"svg"
                                    ,"uglify"
                                    ,"imagemin"
                                    ,"compile-handlebars"
                                    ,"dist-markup"
                                    ,"htmlmin" 
                                    ,"cacheBust"
                                ]);

    
    grunt.registerTask("forcereload", function() {

        console.log( "Changing watched file: " + grunt.config("vars.dist") + "/forcereload.txt" );
        grunt.file.write( grunt.config("vars.dist") + "/forcereload.txt", Math.random() );
    });


    grunt.registerTask("svg", "Generates symbol and converts it to a handlebars file", ["svg_sprite", "copy:svg-to-hbs"])

    grunt.registerTask('dist-markup', function() {
        var distHtml = grunt.file.read( grunt.config("vars.deploy") + "index-uncompressed.html" )
            ,devHtml = grunt.file.read( grunt.config("vars.deploy") + "index.html" ); // handlebars splits this out

        var tagStart = "<!--{GRUNT_BODY_MARKUP_START}-->"
            tagEnd = "<!--{GRUNT_BODY_MARKUP_END}-->";

        var devContent = devHtml.split(tagStart)[1].split(tagEnd)[0];
        var distAll = distHtml.split(tagStart)[0] + tagStart + "\n" + devContent + tagEnd + distHtml.split(tagEnd)[1];

        grunt.file.write( grunt.config("vars.deploy") + "index-uncompressed.html", distAll );
    });

};
