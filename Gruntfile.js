
module.exports = function (grunt) {

    /**
     * This is the project's master Gruntfile, which is responsible only for running
     * a local dev server and triggering other Gruntfiles using 'grunt-hub'.
     * If you simply write `grunt` on the command-line, it will also write out a list of possible commands.
     */

	
    // main build
    grunt.registerTask("build", [ "css", "misc" ]); // "js", 
    grunt.registerTask("b", [ "build" ]);


    // update project dependencies
    grunt.registerTask("update",[ "shell:grunt_update", "shell:bower_update" ]);
	grunt.registerTask("up",    [ "update" ]);
    

    // css options
    grunt.registerTask("css",       [ "hub:sass.both" ]);
    grunt.registerTask("css.debug", [ "hub:sass.debug" ]);
    grunt.registerTask("css.min",   [ "hub:sass.min" ]);

    grunt.registerTask("css.watch",       [ "hub:sass.wboth" ]);
    grunt.registerTask("css.debug.watch", [ "hub:sass.wbug" ]);
    grunt.registerTask("css.min.watch",   [ "hub:sass.wmin" ]);


    // other tasks
    grunt.registerTask("misc",  [ "hub:misc" ]);
    grunt.registerTask("svg",  [ "hub:svg" ]);


    // local dev server
    grunt.registerTask("local",  [ "connect:dev", "watch"]);
	grunt.registerTask("local.dist", [ "connect:dist"]);

    grunt.registerTask("hbs", ["hub:hbs"])

	/**
     * Writes out a list of options for this project to the command-line
     * if someone just comes in and writes `grunt`.
     */
	grunt.registerTask("default", function() {

        grunt.log.header( "Hi there. What would you like to run?".yellow.bold );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt build".yellow);
        grunt.log.writeln("Will build a production ready version of your project.".grey);


        grunt.log.writeln( ""); // LINE BREAK
                    
        
        grunt.log.writeln( "grunt update".yellow + " or ".grey + "grunt up".yellow );
        grunt.log.writeln("Will update your grunt and bower dependencies.".grey );
                    
        
        grunt.log.writeln( ""); // LINE BREAK

        
        grunt.log.writeln( "grunt local".yellow );
        grunt.log.writeln( "Will run a local development server.".grey );
                    
        
        grunt.log.writeln( ""); // LINE BREAK

        
        grunt.log.writeln( "grunt svg".yellow );
        grunt.log.writeln( "Will run generate an svg symbol handlebars file.".grey );
                    
        
        grunt.log.writeln( ""); // LINE BREAK

        
        grunt.log.writeln( "grunt watch:hbs".yellow );
        grunt.log.writeln( "Add a watch for handlebars files.".grey );
                    
        
        grunt.log.writeln( ""); // LINE BREAK

        
        grunt.log.writeln( "grunt local.dist".yellow );
        grunt.log.writeln( "Will run a local server with production-ready 'distribution' resources.".grey );
                    
        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt css".yellow );
        grunt.log.writeln( "Will compile your app's scss to css (both debug & minified versions). \nOther variations are as follows:".grey );
        grunt.log.writeln( "grunt css.debug".yellow + "         - Debug only".grey );
        grunt.log.writeln( "grunt css.min".yellow + "           - Min only".grey );
        grunt.log.writeln( "grunt css.watch".yellow + "         - Both, plus watch".grey );
        grunt.log.writeln( "grunt css.debug.watch".yellow + "   - Debug only, plus watch".grey );
        grunt.log.writeln( "grunt css.min.watch".yellow + "     - Min only, plus watch".grey );
                    

        grunt.log.writeln( ""); // LINE BREAK
                

        grunt.log.writeln( "grunt misc".yellow );
        grunt.log.writeln( "Will run any additional tasks for the project, such as copying images and libraries from '_dev' into '_dist'.".grey );

	});



    // grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-hub');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-shell');


    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json')

        ,paths: {
            dev: "_dev"
            ,dist: "_dist"
            ,app:    "_dev/app/"
            ,deploy: "_dist/deploy/"
            ,devDomain: 'http://<%= connect.options.hostname %>:<%= connect.dev.options.port %>/'
            ,distDomain: 'http://<%= connect.options.hostname %>:<%= connect.dist.options.port %>/'
        }
        

        /**
         * Using grunt-hub to call other Gruntfiles so we can keep our builds organised and portable.
         */
        ,hub: {
            options: {
                concurrent: 3
            }
            
            // other miscellaneous tasks, such as copying files
            ,misc: {
                src: ["grunt_tasks/misc/Gruntfile.js"]
                ,tasks: [ "default" ]
            }

            ,svg: {
                src: ["grunt_tasks/misc/Gruntfile.js"]
                ,tasks: [ "svg" ]
            }

            ,hbs: {
                src: ["grunt_tasks/misc/Gruntfile.js"]
                ,tasks: [ "clean:html", "compile-handlebars" ]
            }

            ,"misc.forcereload": {
                src: ["grunt_tasks/misc/Gruntfile.js"]
                ,tasks: [ "forcereload" ]
            }


            /**
             * Options for Sass
             */

            // Both debug & min css 
            ,"sass.both": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:both" ]
            }
            // Debug only
            ,"sass.debug": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:debug" ]
            }
            // Min only
            ,"sass.min": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:min" ]
            }


            // Both debug & min css, plus watch
            ,"sass.wboth": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wboth" ]
            }
            // Debug & watch
            ,"sass.wbug": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wbug" ]
            }
            // Min & watch
            ,"sass.wmin": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wmin" ]
            }
        }

        

        /**
         * 'keepalive' should be false if 'livereload' is true.
         * Using a custom port seems to screw up livereload as well, so just stick to value of true.
         */
        ,connect: {
            options: {
                hostname: 'localhost'
            },
            dev: {
                options: {
                    keepalive: false,
                    livereload: true,
                    port: 8001,
                    base: "",
                    open: '<%= paths.devDomain %>_dist/deploy/'
                }
            },
            dist: {
                options: {
                    port: 8000,
                    keepalive: true,
                    livereload: false,
                    base: '<%= paths.dist %>/deploy/',
                    open: '<%= paths.distDomain %>'
                }
            }
        },


        watch: {
            html: {
                options: {
                    livereload: true
                },
                files: [
                    '<%= paths.dist %>/forcereload.txt' // change this whenever you want to force a reload
                ]
            },

            hbs: {
                options: {
                    //livereload: true
                },
                files: [
                    '<%= paths.app %>**/*.hbs'
                ],
                tasks: [ "hbs", "hub:misc.forcereload" ]
            }
        }


        ,shell: {
            grunt_update: {
                command: [
                    "npm --prefix grunt_tasks/sass install"
                    ,"npm --prefix grunt_tasks/misc install"
                ].join('&&')
            }
            ,bower_update: {
                command: "bower install"
            }
        }

    });
};