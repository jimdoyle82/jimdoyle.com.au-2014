
(function($, _, Modernizr, jdUtils) {

	var curIntv
	  , tileDur = 1000
	  , $wrapper = $(".tiles-state-wrapper")
	  , CLS_IS_MORE_SHOWN = "is-moreshown";



	addClassesForFixedPos();
	addClassesForBackgroundSize();
	addMultiColumnFallback();
	addTileHandlers();
	addNavHandlers();
	addContactInfo();

	var $inner = $("#content-inner")
	  , $outer = $("#content-outer")
	  , $wrap = $(".tiles-state-wrapper");

	function resize() {
		$outer.css("height", "");

		var innerH = $inner.outerHeight()
		  , outerH = $outer.height()
		
		
		setTimeout(function() {

			var activeTileH = $(".tiles.active").height()
			, activeTileTotalH = activeTileH + $wrap.offset().top;

			var newH;
			if(innerH >= outerH) newH = innerH;
			if(newH && activeTileTotalH >= newH) newH = activeTileTotalH;
			if(newH) $outer.height( newH );
			
			$wrap.css("padding-top", "");
			$wrap.css("padding-top", activeTileH + "px");
		}, 100);


		setTileImgH();
	}

	$(window).on("resize", resize);
	resize();
	setReadMoreHandler();

	$(".app-outterbg").removeClass("is-initializing");

	function setTileImgH() {
		var $this, $container, $hdr, $csl;
		$(".tiles-csl").each(function() {
			$this = $(this);
			$container = $this.closest(".tiles-container");
			$hdr = $container.find(".tiles-heading");
			$csl = $container.find(".tiles-csl");
			
			if($this.hasClass("no-slick"))
				$csl.height( $container.height() - $hdr.outerHeight() );
			else
				$csl.css("height", "");
		});
	}

	function setTilesCarousel($tileList, justKill) {
		
		$tileList.find(".tiles-csl.slick-slider").slick("unslick").addClass("no-slick");

		if(justKill) return;

		$tileList.find(".tiles-csl").removeClass("no-slick").each(function() {

			$(this).on("init", resize); // must come before slick call

			$(this).slick({
				nextArrow: '<button type="button" class="tiles-cslbtn slick-next"><svg class="tiles-cslicon"><use xlink:href="#arrow"></use></svg></button>'
				,prevArrow: '<button type="button" class="tiles-cslbtn slick-prev"><svg class="tiles-cslicon"><use xlink:href="#arrow"></use></svg></button>'
				,dots: true
				,appendDots: $(this).siblings(".tiles-csldots").eq(0)
			});

		});
	}

	function addClassesForFixedPos() {
		/**
		 * Some androids (v0-2) and all windows phones, don't do position:fixed
		 * so they need some love.
		 */

		var and = jdUtils.isAndroid()
			,winPh = jdUtils.isIEMobile();

		if( and || winPh ) {

			$(".appnavbar").removeClass("navbar-fixed-top");
			$(".appfooter").removeClass("navbar-fixed-bottom");
			$("html").addClass("no-fixed-pos");
		}

		/*
		// possibly detect android version and allow fixed positioning on version 3 and above
		getAndroidVersion(); //"4.2.1"
		parseInt(getAndroidVersion()); //4
		parseFloat(getAndroidVersion()); //4.2
		*/
	}


	function addClassesForBackgroundSize() {
		if( !Modernizr.backgroundsize ) {
			$("html").addClass('no-background-size');

			// workaround for hit not being clickable because of polyfill
			$(".tiles-tn").on("click", function(evt) {
				$(evt.target).closest(".tiles").find(".tiles-hit").trigger("click");
			});

			$(".snugbox-img").addClass('hideimg');
			
			// Until doc ready fired ".background-size-polyfill" doesn't exist
			ticker( 10, 500, function() {
				return ($(".tiles .background-size-polyfill").length === 0);
			}, function() {
				$(".snugbox-img").removeClass('hideimg');
				resizeBgSizePolyfill();
			});
		}
	}


	function resizeBgSizePolyfill() {
		if( Modernizr.backgroundsize ) return;

		$.each( $(".tiles .background-size-polyfill"), function(i, el) {

			// set heights, so that IE8 can render the polyfill correctly
			$(el).height( $(el).parent().height() );
			
			// triggers event handlers in polyfill on bg update
			$(el).parent().css("background-position","center center");
		});
	}


	function addContactInfo() {

		// so sneaky bots don't get me
		var $email = $(".contactpanel-email .js-fill");
		$email.text( "info@jimdoyle.com.au" );
		$email.attr( "href", "mailto:info@jimdoyle.com.au" );

		var $phone = $(".contactpanel-phone .js-fill");
		$phone.text( "0414 562 166" );
	}

	function addNavHandlers() {

		var open = function(doOpen, evt) {

			$btnContact[0].classList[ 	( doOpen ? "add" : "remove" ) ]("btncontact-hidden");
			$contactPanel[0].classList[ ( doOpen ? "remove" : "add" ) ]( "contactpanel-hidden" );
			
			if( evt) evt.preventDefault();
		}

		var $contactPanel = $(".contactpanel")
			,$btnContact = $(".btncontact")
			,$navBarCollapse = $(".appnavbar-collapse");

		$navBarCollapse.bind('shown.bs.collapse', function() {
			console.log( arguments );
		});

		$navBarCollapse.bind('hidden.bs.collapse', function() {
			console.log( arguments );
		});

		$btnContact.on("click", function(evt) {
			open($contactPanel.hasClass("contactpanel-hidden"), evt);
			//$navBarCollapse.collapse( "hide");
			evt.preventDefault();
		});

		$(".apptitle").on("click", function(evt) {
			open(false);
			closeTile();
			evt.preventDefault();
		});


		$(".contactpanel-btnclose").on('click', function(evt) { open(false, evt) } );
		$(".contactpanel-title").on('click'	  , function(evt) { open(false, evt) } );
	}

	function addTileHandlers() {
		var $tile;

		$(".tiles-hit").on("click", function(evt) {

			$tile = $(this).closest(".tiles");
			
			if( Modernizr.csstransitions ) {
				$tile.addClass( "activating" );
				$wrapper.addClass("is-expanding");

				clearInterval( curIntv );
				curIntv = setTimeout( function() {
					expandTile( $tile );
				}, tileDur );
			} else {
				expandTile( $tile );
			}

			evt.preventDefault();
		});

		$('.tiles-btnclose').on("click", function(evt) {
			closeTile( $(this).closest('.tiles') );
			evt.preventDefault();
		});

	}

	function expandTile( $tile ) {
		$wrapper.addClass("is-expanded");
		$wrapper.removeClass("is-expanding");
		
		var $allTiles = $(".tiles");
		$allTiles.removeClass(CLS_IS_MORE_SHOWN);
		$allTiles.find(".tiles-heading").addClass("is-clamped");
		setTilesCarousel($allTiles, true);

		$tile.addClass( "active" );
		$wrapper.addClass("is-expanded");
		$tile.find(".tiles-heading").removeClass("is-clamped");
		setTileColumns( true, $tile[0] );
		setOddTileClasses();
		resizeBgSizePolyfill();
		//resize();
		setTilesCarousel($tile);
		setTimeout(resize, 0);
	}

	function collapseTile( $tile ) {
		$wrapper.removeClass("is-expanding");
		$wrapper.removeClass("is-expanded");

		$tile.removeClass( "active" );
		$wrapper.removeClass("is-expanded");
		setTileColumns( false );
		setOddTileClasses( true );
		resizeBgSizePolyfill();
		setTilesCarousel($tile, true);
		resize();
	}


	function closeTile( $tile ) {

		if( !$tile ) $tile = $(".tiles.active").eq(0);

		if( !$tile || $tile.length === 0 ) return;


		if( Modernizr.csstransitions ) {
			

			$tile.addClass("deactivating");
			$wrapper.addClass("is-expanding");

			clearInterval( curIntv );
			curIntv = setTimeout( function() {
				collapseTile( $tile );
			}, tileDur );
		} else {
			collapseTile( $tile );
		}
	}


	function setTileColumns( isExpanded, target ) {

		_.forEach( $(".tiles"), function( el ) {
			
			// gets classes related to columns
			var clsArr = _.where( el.classList, function(cls) {
				return cls.indexOf("col-") === 0;
			});

			// removes collected column classes
			_.forEach( clsArr, function(cls) {
				el.classList.remove( cls );
			});

			if( isExpanded ) {
				if( el === target ) {
					el.classList.add("col-xs-12");
					el.classList.add("col-sm-8");
					el.classList.add("col-lg-6");
				} else {
					el.classList.add("col-xs-6");
					el.classList.add("col-sm-4");
					el.classList.add("col-lg-3");
					el.classList.remove( "active" );
				}
			} else {
				// returns classes to tile view
				el.classList.add("col-xs-12");
				el.classList.add("col-sm-6");
				el.classList.add("col-md-4");
			}

		});
	}

	function setOddTileClasses( justClear ) {
		
		var id = 0;
		_.forEach( $(".tiles"), function( el ) {

			el.classList.remove("odd");

			if( !justClear && !el.classList.contains("active") ) {

				if( id % 2 !== 0 )
					el.classList.add("odd");
				id++;
			}
		
		});	
	}

	function addMultiColumnFallback() {
		if( $.fn.multicolumn ) {
			$(".profilebody").multicolumn({
				columnCount: 2
				,childSelector: ".profilebody-columnfallback"
			});
		} else if( jdUtils.isAndroid() ) {
			/**
			 * Bug in Android 3.2.1 (and maybe others), where column widths are all messed up.
			 * So we just remove it to avoid crappy layout.
			 */
			if( $(".profilebody").css("-webkit-columns") == "" )
				$(".profilebody").css("-webkit-columns", "auto");
		}
	}

	function ticker( countMax, tickDur, ignoreWhileTrueCheck, callback ) {
		var count = 0
			,tick = tickDur
			,intv = setInterval( function() {

				// console.log( "tick", count, ignoreWhileTrueCheck() );

				// attept only up to the max amount, then abort
				if( count >= countMax ) {
					// kill the ticker
					clearInterval( intv );
					return;
				}

				count++;

				// do nothing until polyfill has kicked in
				if( ignoreWhileTrueCheck() ) return;				

				// kill the ticker
				clearInterval( intv );

				callback();
			}, tick);
	}


	function setReadMoreHandler() {
		
		$(".tiles-morebtn").on("click", function() {

			var $tile = $(this).closest(".tiles");
			$tile.toggleClass(CLS_IS_MORE_SHOWN);
			
			resize();
		});
	}

})(jQuery, _, Modernizr, jimd.utils); // "_" is for lodash