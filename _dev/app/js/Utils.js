var jimd = jimd || {};

jimd.utils = (function( Modernizr ) {

	return {

		prefixedEvent: function(element, type, callback, allowChildEvents) {
			/** 
			 * Uses 'element.addEventListener', which is not supported in IE8.
			 * However, 'Modernizr.csstransitions' check stops IE8 before then anyway.
			 */

	        if( !Modernizr.csstransitions ) {
	            if( window.console && window.console.warn )
	                window.console.warn("App.js -> prefixedEvent() animated css transitions not supported.");
	            callback( null );
	            return;
	        }

	        var pfx = ["webkit", "moz", "MS", "o", ""];
	        for (var p = 0; p < pfx.length; p++) {
	            if (!pfx[p]) type = type.toLowerCase();

	            // needs to be true for FF and IE. Something to do with bubbling.
	            var useCapture = true;

	            element.addEventListener(pfx[p]+type, function(evt) {

	                // stops children events
	                if( !allowChildEvents && evt.target !== element ) return;

	                callback(evt);

	            }, useCapture);
	        }
	    }

	    ,getAndroidVersion: function(ua) {
		    var ua = ua || navigator.userAgent
		    	,match = ua.match(/Android\s([0-9\.]*)/);
		    return match ? match[1] : false;
		}

	    ,isAndroid: function(ua) {
		    var ua = ua || navigator.userAgent
		    	,regExp = new RegExp("Android", "i");
		    return ua.match(regExp);
		}

		,isIEMobile: function(ua) {
		    var ua = ua || navigator.userAgent 
		    	,regExp = new RegExp("IEMobile", "i");
		    return ua.match(regExp);
		}
	}
})(Modernizr);